package com.neeraj.roposo.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * Shared Preference class that help to save
 */
@SuppressWarnings({"UnusedDeclaration"})
public class AppSharedPreference {

    private SharedPreferences mPrefs;
    private Editor mPrefsEditor;

    private final String IS_FOLLOWING = "is_following";
    private static AppSharedPreference mInstance;


    public static AppSharedPreference getInstance(Context context) {

        if (mInstance == null) {
            mInstance = new AppSharedPreference(context);
        }
        return mInstance;
    }

    private AppSharedPreference(Context context) {
        String TAG = this.getClass().getSimpleName();
        this.mPrefs = context.getSharedPreferences(TAG, Context.MODE_PRIVATE);
        this.mPrefsEditor = mPrefs.edit();
        mPrefsEditor.apply();
    }

    public void setIsFollowing(boolean following) {
        mPrefsEditor.putBoolean(IS_FOLLOWING, following);
        mPrefsEditor.apply();
    }

    public boolean getIsFollowing() {
        return mPrefs.getBoolean(IS_FOLLOWING, false);
    }

    public void clearData() {
        mPrefsEditor.clear();
        mPrefsEditor.apply();
    }
}
