package com.neeraj.roposo.activity;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.neeraj.roposo.R;
import com.neeraj.roposo.common.AppSharedPreference;
import com.neeraj.roposo.model.Desc;
import com.pkmmte.view.CircularImageView;
import com.squareup.picasso.Picasso;

/**
 * <p>
 * Story Detail class help to show detail information of particular story that contains
 * author name, title, description, follow button etc.
 * User can follow or Un-follow in detail screen  too.
 * </p>
 */
@SuppressWarnings({"UnusedDeclaration"})
public class StoryDetailActivity extends AppCompatActivity implements OnClickListener {

    public static final String STORY_DETAIL = "story_detail";
    public static final String PROFILE_IMAGE = "user_image";
    public static final String USER_NAME = "user_name";
    private Desc mDescription;
    private Button mBtnFollow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story_detail);

        // setup a toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_act_story_detail);
        setSupportActionBar(toolbar);
        //noinspection ConstantConditions
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        // initialize view
        CircularImageView imgProfilePic = (CircularImageView) findViewById(R.id.cimg_vw_view_user_info_profile_image);
        TextView txvAuthor = (TextView) findViewById(R.id.txv_view_user_info_author);
        TextView txvTitle = (TextView) findViewById(R.id.txv_view_user_info_title);
        mBtnFollow = (Button) findViewById(R.id.btn_view_user_info_follow);
        TextView txvDesc = (TextView) findViewById(R.id.txv_view_user_info_desc);
        TextView txvLikes = (TextView) findViewById(R.id.txv_view_user_info_likes);
        TextView txvComment = (TextView) findViewById(R.id.txv_view_user_info_comments);
        ImageView txvMainImage = (ImageView) findViewById(R.id.img_vw_view_user_info);

        // get data from intent
        if (getIntent().hasExtra(STORY_DETAIL)) {
            mDescription = getIntent().getParcelableExtra(STORY_DETAIL);
            Picasso.with(this).load(getIntent().getStringExtra(PROFILE_IMAGE)).placeholder(R.drawable.ic_user_placeholder).into(imgProfilePic);
            txvAuthor.setText(getIntent().getStringExtra(USER_NAME));
        }


        // set data
        txvTitle.setText(mDescription.title);
        if (!TextUtils.isEmpty(mDescription.description)) {
            txvDesc.setVisibility(View.VISIBLE);
            txvDesc.setText(mDescription.description);
        } else {
            txvDesc.setVisibility(View.GONE);
        }

        if (!AppSharedPreference.getInstance(this).getIsFollowing()) {
            mBtnFollow.setBackgroundResource(R.drawable.shp_round_rect_dark_red);
            mBtnFollow.setText(R.string.follow);
        } else {
            mBtnFollow.setBackgroundResource(R.drawable.shp_round_rect_red);
            mBtnFollow.setText(R.string.unfollow);
        }

        txvLikes.setText(mDescription.likes_count + getString(R.string.likes));
        txvComment.setText(mDescription.comment_count + getString(R.string.comments));
        Picasso.with(this).load(mDescription.si).into(txvMainImage);

        // set click listener
        mBtnFollow.setOnClickListener(this);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_view_user_info_follow:
                if (AppSharedPreference.getInstance(this).getIsFollowing()) {
                    // show alert to user when un-following
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage(R.string.are_you_sure_you_want_to_unfollow).setCancelable(true)
                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    mBtnFollow.setBackgroundResource(R.drawable.shp_round_rect_dark_red);
                                    mBtnFollow.setText(R.string.follow);
                                    AppSharedPreference.getInstance(StoryDetailActivity.this).setIsFollowing(false);
                                    dialog.dismiss();
                                }
                            })

                            .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    mBtnFollow.setBackgroundResource(R.drawable.shp_round_rect_red);
                    mBtnFollow.setText(R.string.unfollow);
                    AppSharedPreference.getInstance(this).setIsFollowing(true);
                }
                break;
        }
    }
}
