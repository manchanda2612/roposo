package com.neeraj.roposo.adapter;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.neeraj.roposo.R;
import com.neeraj.roposo.common.AppSharedPreference;
import com.neeraj.roposo.model.Data;
import com.neeraj.roposo.model.Desc;
import com.pkmmte.view.CircularImageView;
import com.squareup.picasso.Picasso;
import java.util.List;

/**
 * <p>
 * Story Adapter class help to show list of stories to user that comprises of user profile image,
 * author name, title, description etc.
 * User can click on title to show detail of particular story
 * </p>
 */
@SuppressWarnings({"UnusedDeclaration"})
public class StoryAdapter extends RecyclerView.Adapter<StoryAdapter.StoryHolder>  {

    private Data mData;
    private List<Desc> mDataList;
    private Context mContext;
    private StoryAdapterClickListener mStoryAdapterClickListener;

    /**
     * Constructor that initialize story data and list and set reference for listener
     * @param context context of story class
     * @param data story data
     * @param adapterClickListener set listener reference
     */
    public StoryAdapter(Context context, Data data, StoryAdapterClickListener adapterClickListener) {
        mContext = context;
        mData = data;
        mDataList = data.desc;
        mStoryAdapterClickListener = adapterClickListener;

    }

    @Override
    public StoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mLayoutInflater = LayoutInflater.from(parent.getContext());
        View view = mLayoutInflater.inflate(R.layout.adapter_story, parent, false);
        return new StoryHolder(view);
    }

    @Override
    public void onBindViewHolder(final StoryHolder holder, int position) {

        Desc feedData = mDataList.get(position);

        Picasso.with(mContext).load(mData.image).placeholder(R.drawable.ic_user_placeholder).into(holder.mImgProfilePic);

        holder.mTxvAuthor.setText(mData.username);
        holder.mTxvTitle.setText(feedData.title);

        if(!TextUtils.isEmpty(feedData.description)) {
            holder.mTxvDescription.setVisibility(View.VISIBLE);
            holder.mTxvDescription.setText(feedData.description);
        } else {
            holder.mTxvDescription.setVisibility(View.GONE);
        }

        if(!AppSharedPreference.getInstance(mContext).getIsFollowing()) {
            holder.mBtnFollow.setBackgroundResource(R.drawable.shp_round_rect_dark_red);
            holder.mBtnFollow.setText(R.string.follow);
        } else {
            holder.mBtnFollow.setBackgroundResource(R.drawable.shp_round_rect_red);
            holder.mBtnFollow.setText(R.string.unfollow);
        }
        holder.mBtnFollow.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AppSharedPreference.getInstance(mContext).getIsFollowing()) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setMessage(R.string.are_you_sure_you_want_to_unfollow).setCancelable(true)
                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    holder.mBtnFollow.setBackgroundResource(R.drawable.shp_round_rect_dark_red);
                                    holder.mBtnFollow.setText(R.string.follow);
                                    AppSharedPreference.getInstance(mContext).setIsFollowing(false);
                                    if(null != mStoryAdapterClickListener) mStoryAdapterClickListener.onFollowBtnClick();
                                    dialog.dismiss();
                                }
                            })

                            .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    AlertDialog alert = builder.create();
                    alert.show();

                } else {
                    holder.mBtnFollow.setBackgroundResource(R.drawable.shp_round_rect_red);
                    holder.mBtnFollow.setText(R.string.unfollow);
                    AppSharedPreference.getInstance(mContext).setIsFollowing(true);
                    if(null != mStoryAdapterClickListener) mStoryAdapterClickListener.onFollowBtnClick();
                }

            }
        });

        Picasso.with(mContext).load(feedData.si).into(holder.mImgMainImage);

        holder.mTxvLikes.setText(feedData.likes_count + mContext.getString(R.string.likes));
        holder.mTxvComments.setText(feedData.comment_count + mContext.getString(R.string.comments));

    }

    @Override
    public int getItemCount() {
        return (null != mDataList ? mDataList.size() : 0);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    /**
     * View holder class that help to initialize view for an item.
     */
    public class StoryHolder extends RecyclerView.ViewHolder implements OnClickListener{
        TextView mTxvAuthor;
        TextView mTxvDescription;
        TextView mTxvTitle;
        Button mBtnFollow;
        CircularImageView mImgProfilePic;
        ImageView mImgMainImage;
        TextView mTxvLikes;
        TextView mTxvComments;


        public StoryHolder(View itemView) {
            super(itemView);
            mTxvTitle = (TextView) itemView.findViewById(R.id.txv_view_user_info_title);
            mTxvDescription = (TextView) itemView.findViewById(R.id.txv_view_user_info_desc);
            mTxvAuthor = (TextView) itemView.findViewById(R.id.txv_view_user_info_author);
            mBtnFollow = (Button) itemView.findViewById(R.id.btn_view_user_info_follow);
            mImgProfilePic = (CircularImageView) itemView.findViewById(R.id.cimg_vw_view_user_info_profile_image);
            mImgMainImage = (ImageView) itemView.findViewById(R.id.img_vw_view_user_info);
            mTxvLikes = (TextView) itemView.findViewById(R.id.txv_view_user_info_likes);
            mTxvComments = (TextView) itemView.findViewById(R.id.txv_view_user_info_comments);
            mTxvTitle.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(null != mStoryAdapterClickListener) mStoryAdapterClickListener.onItemClick(getAdapterPosition() ,mDataList.get(getAdapterPosition()));
        }
    }

    /**
     * Listener that provide two method that help to pass event to subscriber
     */
    public interface StoryAdapterClickListener {
        void onItemClick(int position, Desc desc);
        void onFollowBtnClick();
    }

}
