package com.neeraj.roposo.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.neeraj.roposo.R;
import com.neeraj.roposo.adapter.StoryAdapter;
import com.neeraj.roposo.adapter.StoryAdapter.StoryAdapterClickListener;
import com.neeraj.roposo.common.AppSharedPreference;
import com.neeraj.roposo.common.VerticalSpaceForRecyclerView;
import com.neeraj.roposo.model.Data;
import com.neeraj.roposo.model.Desc;
import com.neeraj.roposo.util.GsonUtils;
import com.neeraj.roposo.util.StringUtils;
import com.pkmmte.view.CircularImageView;
import com.squareup.picasso.Picasso;

/**
 * <p>
 * Story Activity help user to show story information of particular user including its name, handle, follower count, image,
 * its list of stories that comprises its title, desc, likes count and comments counts etc.
 * User can see detail of each story and follow and un-follow a user.
 * </p>
 */
@SuppressWarnings({"UnusedDeclaration"})
public class StoryActivity extends AppCompatActivity implements OnClickListener, StoryAdapterClickListener {

    private TextView mTxvUserName;
    private TextView mTxvHandle;
    private TextView mTxvFollower;
    private TextView mTxvAbout;
    private Button mBtnFollower;
    private CircularImageView mImgProfilePic;
    private StoryAdapter mStoryAdapter;
    private RecyclerView mListView;
    private String mProfileImage;
    private int mPosition;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story);

        // setup a toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_act_main);
        setSupportActionBar(toolbar);

        // initialize view
        mTxvUserName = (TextView) findViewById(R.id.txv_act_main_name);
        mTxvHandle = (TextView) findViewById(R.id.txv_act_main_info);
        mTxvFollower = (TextView) findViewById(R.id.txv_act_main_follower);
        mTxvAbout = (TextView) findViewById(R.id.txv_act_main_about);
        mBtnFollower = (Button) findViewById(R.id.btn_act_main_follower);
        mImgProfilePic = (CircularImageView) findViewById(R.id.cimg_vw_act_main_profile_pic);
        mListView = (RecyclerView) findViewById(R.id.rcl_act_main);
        mListView.setLayoutManager(new LinearLayoutManager(this));
        mListView.addItemDecoration(new VerticalSpaceForRecyclerView(20));

        // fetch data from json
        Data storyData = GsonUtils.fromJson(StringUtils.loadJSONFromAsset(this, "data.json"), Data.class);

        // set data into ui
        setDataIntoUI(storyData);

        // set listener
        mBtnFollower.setOnClickListener(this);

    }

    /**
     * Method help to set data into ui
     * @param storyData complete data of user
     */
    private void setDataIntoUI(Data storyData) {

        mProfileImage = storyData.image;

        mTxvUserName.setText(storyData.username);
        mTxvHandle.setText(storyData.handle);
        mTxvFollower.setText(storyData.followers + getString(R.string.followers));
        mTxvAbout.setText(storyData.about);

        if (!AppSharedPreference.getInstance(this).getIsFollowing()) {
            mBtnFollower.setBackgroundResource(R.drawable.shp_round_rect_dark_red);
            mBtnFollower.setText(R.string.follow);
        } else {
            mBtnFollower.setBackgroundResource(R.drawable.shp_round_rect_red);
            mBtnFollower.setText(R.string.unfollow);
        }


       Picasso.with(this).load(storyData.image).placeholder(R.drawable.ic_user_placeholder).into(mImgProfilePic);

        // set Adapter
        mStoryAdapter = new StoryAdapter(this, storyData, this);
        mListView.setAdapter(mStoryAdapter);
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (!AppSharedPreference.getInstance(this).getIsFollowing()) {
            mBtnFollower.setBackgroundResource(R.drawable.shp_round_rect_dark_red);
            mBtnFollower.setText(R.string.follow);
        } else {
            mBtnFollower.setBackgroundResource(R.drawable.shp_round_rect_red);
            mBtnFollower.setText(R.string.unfollow);
        }

        // when user come back to story list screen
        // show previous item as in real roposo app
        mListView.scrollToPosition(mPosition);
        mListView.setAdapter(mStoryAdapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_act_main_follower:
                if (AppSharedPreference.getInstance(this).getIsFollowing()) {

                    // show alert to user before un-following
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage(R.string.are_you_sure_you_want_to_unfollow)
                            .setCancelable(true)
                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    mBtnFollower.setBackgroundResource(R.drawable.shp_round_rect_dark_red);
                                    mBtnFollower.setText(R.string.follow);
                                    AppSharedPreference.getInstance(StoryActivity.this).setIsFollowing(false);
                                    mListView.setAdapter(mStoryAdapter);
                                    dialog.dismiss();
                                }
                            })

                            .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    AlertDialog alert = builder.create();
                    alert.show();

                } else {
                    mBtnFollower.setBackgroundResource(R.drawable.shp_round_rect_red);
                    mBtnFollower.setText(R.string.unfollow);
                    AppSharedPreference.getInstance(this).setIsFollowing(true);
                    mListView.setAdapter(mStoryAdapter);
                }

                break;
        }

    }

    @Override
    public void onItemClick(int position, Desc desc) {

        if(position == 0) {
            mPosition = position;
        } else {
            mPosition = position - 1;
        }

        Intent intent = new Intent(this, StoryDetailActivity.class);
        intent.putExtra(StoryDetailActivity.USER_NAME, mTxvUserName.getText());
        intent.putExtra(StoryDetailActivity.PROFILE_IMAGE, mProfileImage);
        intent.putExtra(StoryDetailActivity.STORY_DETAIL, desc);
        startActivity(intent);
    }

    @Override
    public void onFollowBtnClick() {
        if (!AppSharedPreference.getInstance(this).getIsFollowing()) {
            mBtnFollower.setBackgroundResource(R.drawable.shp_round_rect_dark_red);
            mBtnFollower.setText(R.string.follow);
        } else {
            mBtnFollower.setBackgroundResource(R.drawable.shp_round_rect_red);
            mBtnFollower.setText(R.string.unfollow);
        }
    }
}
