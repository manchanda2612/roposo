package com.neeraj.roposo.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Object model class of description
 */
@SuppressWarnings({"UnusedDeclaration"})
public class Desc implements Parcelable{

    public String description;
    public String id;
    public String verb;
    public String db;
    public String url;
    public String si;
    public String type;
    public String title;
    public boolean like_flag;
    public int likes_count;
    public int comment_count;

    protected Desc(Parcel in) {
        description = in.readString();
        id = in.readString();
        verb = in.readString();
        db = in.readString();
        url = in.readString();
        si = in.readString();
        type = in.readString();
        title = in.readString();
        likes_count = in.readInt();
        comment_count = in.readInt();
    }

    public static final Creator<Desc> CREATOR = new Creator<Desc>() {
        @Override
        public Desc createFromParcel(Parcel in) {
            return new Desc(in);
        }

        @Override
        public Desc[] newArray(int size) {
            return new Desc[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(description);
        parcel.writeString(id);
        parcel.writeString(verb);
        parcel.writeString(db);
        parcel.writeString(url);
        parcel.writeString(si);
        parcel.writeString(type);
        parcel.writeString(title);
        parcel.writeInt(likes_count);
        parcel.writeInt(comment_count);
    }
}
