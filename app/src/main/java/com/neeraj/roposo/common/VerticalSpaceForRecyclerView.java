package com.neeraj.roposo.common;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class VerticalSpaceForRecyclerView extends RecyclerView.ItemDecoration {

    private int mVerticalSpaceHeight;

    public VerticalSpaceForRecyclerView(int verticalSpaceHeight) {
        this.mVerticalSpaceHeight = verticalSpaceHeight;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.bottom = mVerticalSpaceHeight;
    }
}
