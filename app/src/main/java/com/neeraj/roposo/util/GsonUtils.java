package com.neeraj.roposo.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * A Utility class the provide method to fetch data from json
 * User can get json and set to data object class.
 */
@SuppressWarnings({"UnusedDeclaration"})
public class GsonUtils {

	public static <T> T fromJson(String jsonString, Class<T> classType) {

		Gson gson = new GsonBuilder().create();
		return gson.fromJson(jsonString, classType);
	}

}
