package com.neeraj.roposo.util;

import android.app.Activity;

import java.io.IOException;
import java.io.InputStream;

/**
 * A Utility class the help to fetch data from assets folder file
 */
@SuppressWarnings({"UnusedDeclaration"})
public class StringUtils {

    public static String loadJSONFromAsset(Activity activity, String fileName) {
        String json;
        try {
            InputStream is = activity.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            //noinspection ResultOfMethodCallIgnored
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


}
