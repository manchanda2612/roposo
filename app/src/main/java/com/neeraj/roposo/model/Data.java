package com.neeraj.roposo.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Object model class of data
 */
@SuppressWarnings({"UnusedDeclaration"})
public class Data implements Parcelable{

    public  String about;
    public  String id;
    public  String username;
    public  Long followers;
    public  Long following;
    public  String image;
    public  String url;
    public  String handle;
    public  boolean is_following;
    public  Long createdOn;
    public ArrayList<Desc> desc;

    protected Data(Parcel in) {
        about = in.readString();
        id = in.readString();
        username = in.readString();
        image = in.readString();
        url = in.readString();
        handle = in.readString();
    }

    public static final Creator<Data> CREATOR = new Creator<Data>() {
        @Override
        public Data createFromParcel(Parcel in) {
            return new Data(in);
        }

        @Override
        public Data[] newArray(int size) {
            return new Data[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(about);
        parcel.writeString(id);
        parcel.writeString(username);
        parcel.writeString(image);
        parcel.writeString(url);
        parcel.writeString(handle);
    }
}
